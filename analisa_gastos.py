#!/usr/bin/env python
# -*- coding: utf-8 -*-

################################################################################
# Copyright (C) 2018 Rafael Siqueira Telles Vieira. All Rights Reserved.       #
#                                                                              #
# Redistribution and use in source and binary forms, with or without           #
# modification, are permitted provided that the following conditions are met:  #
#                                                                              #
# 1. Redistributions of source code must retain the above copyright notice,    #
# this list of conditions and the following disclaimer.                        #
#                                                                              #
# 2. Redistributions in binary form must reproduce the above copyright notice, #
# this list of conditions and the following disclaimer in the documentation    #
# and/or other materials provided with the distribution.                       #
#                                                                              # 
# 3. The name of the author may not be used to endorse or promote products     #
# derived from this software without specific prior written permission.        #
#                                                                              #
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,     #
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND #
# FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT SHALL THE AUTHOR #
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR       #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF         #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS     #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN      #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)      #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE   #
# POSSIBILITY OF SUCH DAMAGE.                                                  #
#                                                                              #
################################################################################
 

####################################################################
#                        todos orgaos (codigoOS)                   #
####################################################################
'''
63000 - ADVOCACIA-GERAL DA UNIAO
29000 - DEFENSORIA PUBLICA DA UNIAO
57000 - M.DAS MULH.,DA IG.RACIAL DA JUVENT.E DIR.HUM.
22000 - MINIST. DA AGRICUL.,PECUARIA E ABASTECIMENTO
28000 - MINIST. DA INDUSTRIA, COM.EXTERIOR E SERVICOS
20113 - MINIST. DO PLANEJAMENTO, DESENVOLV. E GESTAO
24000 - MINIST.DA CIENCIA,TECNOL.,INOV.E COMUNICACOES
39000 - MINIST.DOS TRANSP.,PORTOS E AVIACAO CIVIL
42000 - MINISTERIO DA CULTURA
52000 - MINISTERIO DA DEFESA
26000 - MINISTERIO DA EDUCACAO
25000 - MINISTERIO DA FAZENDA
53000 - MINISTERIO DA INTEGRACAO NACIONAL
30000 - MINISTERIO DA JUSTICA E SEGURANCA PUBLICA
58000 - MINISTERIO DA PESCA E AQUICULTURA
33000 - MINISTERIO DA PREVIDENCIA SOCIAL
36000 - MINISTERIO DA SAUDE
37000 - MINISTERIO DA TRANSPARENCIA E CGU
56000 - MINISTERIO DAS CIDADES
41000 - MINISTERIO DAS COMUNICACOES
35000 - MINISTERIO DAS RELACOES EXTERIORES
32000 - MINISTERIO DE MINAS E ENERGIA
49000 - MINISTERIO DO DESENVOLVIMENTO AGRARIO
55000 - MINISTERIO DO DESENVOLVIMENTO SOCIAL
51000 - MINISTERIO DO ESPORTE
44000 - MINISTERIO DO MEIO AMBIENTE
40000 - MINISTERIO DO TRABALHO
38000 - MINISTERIO DO TRABALHO E EMPREGO
54000 - MINISTERIO DO TURISMO
81000 - MINISTERIO DOS DIREITOS HUMANOS
20000 - PRESIDENCIA DA REPUBLICA
'''

TODOS_ORGAOS = ["63000", "29000", "57000", "22000", "28000",
                "20113", "24000", "39000", "42000", "52000",
                "26000", "25000", "53000", "30000", "58000",
                "33000", "36000", "37000", "56000", "41000",
                "35000", "32000", "49000", "55000", "51000", 
                "44000", "40000", "38000", "54000", "81000", 
                "20000"]

####################################################################
#                     todos as despesas (codigoED)                 #
####################################################################

TODAS_DESPESAS = {
"82" : "APORTE DE RECURSOS PELO PARCEIRO PUBLICO EM FAVOR DO PARCEIRO PRIVADO DECORRENTE DE CONTRATO DE PARCERIA PUBLICO-PRIVADA - PPP",
"01" : "APOSENT.RPPS, RESER. REMUNER. E REFOR.MILITAR",
"53" : "APOSENTADORIAS DO RGPS - AREA RURAL",
"54" : "APOSENTADORIAS DO RGPS - AREA URBANA",
"62" : "AQUISICAO DE BENS PARA REVENDA",
"61" : "AQUISICAO DE IMOVEIS",
"63" : "AQUISICAO DE TITULOS DE CREDITO",
"64" : "AQUISICAO TIT.REPRES.DE CAP. JA INTEGRALIZADO",
"38" : "ARRENDAMENTO MERCANTIL",
"18" : "AUXILIO FINANCEIRO A ESTUDANTES",
"20" : "AUXILIO FINANCEIRO A PESQUISADORES",
"46" : "AUXILIO-ALIMENTACAO",
"19" : "AUXILIO-FARDAMENTO",
"42" : "AUXILIOS-FUNDO A FUNDO",
"49" : "AUXILIO-TRANSPORTE",
"06" : "BENEFICIO MENSAL AO DEFICIENTE E AO IDOSO",
"73" : "CM OU CAMBIAL DA DIVIDA CONTRATUAL RESGATADA",
"98" : "COMPENSACOES AO RGPS",
"66" : "CONCESSAO DE EMPRESTIMOS E FINANCIAMENTOS",
"65" : "CONSTIT. OU AUMENTO DE CAPITAL DE EMPRESAS",
"04" : "CONTRATACAO POR TEMPO DETERMINADO",
"07" : "CONTRIB. A ENTIDADES FECHADAS DE PREVIDENCIA",
"41" : "CONTRIBUICOES",
"74" : "COR.MONET.E CAMBIAL DA DIV.MOBIL.RESGATADA",
"75" : "COR.MONET.OPER.DE CRED.POR ANTEC.DA RECEITA",
"67" : "DEPOSITOS COMPULSORIOS",
"92" : "DESPESAS DE EXERCICIOS ANTERIORES",
"84" : "DESPESAS DECORRENTES DA PARTICIPACAO EM FUNDOS, ORGANISMOS, OU ENTIDADES ASSEMELHADAS, NACIONAIS E INTERNACIONAIS",
"83" : "DESPESAS DECORRENTES DE CONTRATO DE PARCERIA PUBLICOPRIVADA - PPP, EXCETO SUBVENCOES ECONOMICAS, APORTE E FUNDO GARANTIDOR",
"14" : "DIARIAS - CIVIL",
"15" : "DIARIAS - PESSOAL MILITAR",
"81" : "DISTRIBUICAO DE RECEITAS",
"29" : "DIVIDENDOS - EMPRESAS ESTATAIS DEPENDENTES",
"27" : "ENC.P/ HONRA DE AVAIS, GARANT.,SEGUROS E SIM.",
"25" : "ENC.SOBRE OPER.DE CRED.POR ANTEC. DA RECEITA",
"52" : "EQUIPAMENTOS E MATERIAL PERMANENTE",
"95" : "INDENIZACAO PELA EXECUCAO TRABALHOS DE CAMPO",
"93" : "INDENIZACOES E RESTITUICOES",
"94" : "INDENIZACOES TRABALHISTAS",
"21" : "JUROS SOBRE A DIVIDA POR CONTRATO - LC141/12",
"23" : "JUROS,DESAGIOS E DESCONTOS DA DIV. MOBILIARIA",
"37" : "LOCACAO DE MAO-DE-OBRA",
"30" : "MATERIAL DE CONSUMO",
"32" : "MATERIAL, BEM OU SERVICO P/ DISTRIB. GRATUITA",
"51" : "OBRAS E INSTALACOES",
"47" : "OBRIGACOES  TRIBUTARIAS E  CONTRIBUTIVAS",
"26" : "OBRIGACOES DECORRENTES DE POLITICA MONETARIA",
"13" : "OBRIGACOES PATRONAIS",
"34" : "OUTRAS DESPESAS DE PESSOAL - TERCEIRIZACAO",
"16" : "OUTRAS DESPESAS VARIAVEIS - PESSOAL CIVIL",
"17" : "OUTRAS DESPESAS VARIAVEIS - PESSOAL MILITAR",
"48" : "OUTROS AUXILIOS FINANCEIROS A PESSOA FISICA",
"08" : "OUTROS BENEF.ASSIST.DO SERVIDOR E DO MILITAR",
"57" : "OUTROS BENEFICIOS DO RGPS - AREA RURAL",
"58" : "OUTROS BENEFICIOS DO RGPS - AREA URBANA",
"05" : "OUTROS BENEFICIOS PREVIDENCIARIOS DO RPPS",
"24" : "OUTROS ENCARGOS SOBRE A DIVIDA MOBILIARIA",
"22" : "OUTROS ENCARGOS SOBRE A DIVIDA POR CONTRATO",
"36" : "OUTROS SERVICOS DE TERCEIROS - PESSOA FISICA",
"39" : "OUTROS SERVICOS DE TERCEIROS-PESSOA JURIDICA",
"33" : "PASSAGENS E DESPESAS COM LOCOMOCAO",
"55" : "PENSOES DO RGPS - AREA RURAL",
"56" : "PENSOES DO RGPS - AREA URBANA",
"59" : "PENSOES ESPECIAIS",
"03" : "PENSOES, EXCLUSIVE DO RGPS",
"31" : "PREMIACOES CULTURAIS, ARTISTICAS, CIENTIFICAS",
"77" : "PRINCIPAL CORRIGIDO DIVIDA CONTRATUAL RESG.",
"71" : "PRINCIPAL DA DIVIDA CONTRATUAL RESGATADO",
"76" : "PRINCIPAL DA DIVIDA MOBILIARIA REFINANCIADA",
"72" : "PRINCIPAL DA DIVIDA MOBILIARIA RESGATADA",
"70" : "RATEIO PELA PARTICIPACAO EM CONSORCIO PUBLICO",
"99" : "REGIME DE EXECUCAO ESPECIAL",
"28" : "REMUNERACAO DE COTAS DE FUNDOS AUTARQUICOS",
"96" : "RESSARC. DE DESPESAS DE PESSOAL REQUISITADO",
"09" : "SALARIO-FAMILIA",
"10" : "SEGURO DESEMPREGO E ABONO SALARIAL",
"91" : "SENTENCAS JUDICIAIS",
"35" : "SERVICOS DE CONSULTORIA",
"45" : "SUBVENCOES ECONOMICAS",
"43" : "SUBVENCOES SOCIAIS",
"12" : "VENCIMENTOS E VANTAGENS FIXAS - PES. MILITAR",
"11" : "VENCIMENTOS E VANTAGENS FIXAS - PESSOAL CIVIL"} 

from calendar import monthrange
from bs4 import BeautifulSoup
import requests, sys


ANO = 2017
ORGAO = TODOS_ORGAOS 
LINK = "http://www.portaltransparencia.gov.br/despesasdiarias/resultado?consulta=avancada"
DESPESA = "46"
TIPO = "PAG" # EMP: empenho; LIQ: liquidacao; e PAG: pagamento
CABECALHO = {"User-Agent": "Mozilla/4.0 (compatible; MSIE 6.0; Windows XP 5.1) Lobo/0.98.4"}

# ajusta codificacao
reload(sys)
sys.setdefaultencoding("utf-8")

valor_total = 0
for o in ORGAO:
   # todos os meses
   for i in range(12):
      pagina = 1
      mes = i + 1
      dias = monthrange(ANO, mes)[1]
      if mes < 10:
         mes = "0" + str(mes)
      else:
         mes = str(mes)
      periodoInicio   =    "01"   + "%2F" + mes + "%2F" + str(ANO)
      periodoFim      = str(dias) + "%2F" + mes + "%2F" + str(ANO)
      fase            = TIPO
      codigoOS        = o
      codigoOrgao     = "TOD" # todos
      codigoUG        = "TOD" # todos
      codigoED        = DESPESA

      consulta  = LINK
      consulta += "&periodoInicio=" + periodoInicio
      consulta += "&periodoFim="    + periodoFim 
      consulta += "&fase="          + fase      
      consulta += "&codigoOS="      + codigoOS 
      consulta += "&codigoOrgao="   + codigoOrgao
      consulta += "&codigoUG="      + codigoUG  
      consulta += "&codigoED="      + codigoED 
      consulta += "&codigoFavorecido="
      consulta += "&pagina="        + str(pagina) 

      #print (consulta)
      web  = requests.get(consulta, headers=CABECALHO)
      sopa = BeautifulSoup(web.content, "html.parser")
      campo = sopa.find("span", attrs = {"class": "paginaXdeN"})
      total_paginas = 1
      if campo != None:
         total_paginas = int(campo.text.split()[-1])

      resultado  = sopa.find_all("tr", attrs = {"class": "impar"})
      resultado += sopa.find_all("tr", attrs = {"class": "par"})
      # todas as paginas de consulta
      while pagina <= total_paginas:
         for linha in resultado:
            coluna = linha.find_all("td")
            if len(coluna) < 10:
               break
            campo = coluna[7].text
            campo = campo.strip()
            campo = campo.replace("\n", "")
            campo = campo.replace("\r", "")
            if campo != "Múltiplo":
               saida = ""
               for j in range(len(coluna)):
                  campo = coluna[j].text
                  campo = campo.strip()
                  campo = campo.replace("\n", "")
                  campo = campo.replace("\r", "")
                  saida += campo + "\t"
                  if j == (len(coluna)-1):
                     valor_total += float(campo)
               print (saida.encode("utf-8"))

         pagina += 1
         if pagina <= total_paginas:
            consulta = consulta.replace("pagina=" + str(pagina-1), "pagina=" + str(pagina))
            #print (consulta)
            web  = requests.get(consulta, headers=CABECALHO)
            sopa = BeautifulSoup(web.content, "html.parser")
            resultado  = sopa.find_all("tr", attrs = {"class": "impar"})
            resultado += sopa.find_all("tr", attrs = {"class": "par"})

print ("VALOR TOTAL GASTO: \t" + str(valor_total)); 
